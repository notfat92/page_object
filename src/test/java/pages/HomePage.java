package pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;
import static tests.BaseTest.config;

public class HomePage {

    private static final By CITY = By.xpath("//input[@class=\"ce45093752\"]");
    private static final By FIRST_IN_DROPDOWN = By.xpath("//div[@class=\"a40619bfbe\"]");
    private static final By COOKIES_BUTTON = By.xpath("//button[@id=\"onetrust-accept-btn-handler\"]");
    private static final By FIRST_DATE = By.xpath("//span[@data-date=\"2023-05-10\"]");
    private static final By LAST_DATE = By.xpath("//span[@data-date=\"2023-05-15\"]");
    private static final By SEARCH = By.xpath("//span[.='Найти']");

    public HomePage openPage() {
        open(config.baseUrl());
        return this;
    }
    public HomePage acceptCookies() {
        $(COOKIES_BUTTON).click();
        return this;
    }

    public HomePage findByCity(String cityName) {
        $(CITY).sendKeys(cityName);
        $(FIRST_IN_DROPDOWN).shouldHave(Condition.text(cityName));
        $(FIRST_IN_DROPDOWN).click();
        $(FIRST_DATE).click();
        $(LAST_DATE).click();
        $(SEARCH).click();
        return this;
    }
}