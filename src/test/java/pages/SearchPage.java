package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selenide.*;
import static tests.BaseTest.config;

public class SearchPage {

    private static final By MAP = By.xpath("//*[@id=\"right\"]/div[1]/div/div/div/span/span");
    private static final By MARKER_HOTEL = By.xpath("//*[@id=\"b_map_tiles\"]/div/div/div[2]/div[2]/div/div[3]/div[1]");

   // private static final By HOTEL = By.xpath("//a[@data-marker-id='23739']");



    public SearchPage pageSearch () {
      $(MAP).click();
      return this;
    }
    public SearchPage selectHotel () {
        $(MARKER_HOTEL).click();
        return this;
    }

}




