package pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class HotelPage {

    private static final By STARS_RATING = By.xpath("//span[@class='cb5ebe3ffb c7b191f510']");
    private static final By HOTEL_PAGE_NAME = By.xpath("//h2[@class='d2fee87262 pp-header__title']");

    public static final By FIRST_HOTEL_NAME = By.xpath("//div[@class='map_left_cards__container']/div/a/div/div/span");
    public static final By FIRST_HOTEL_RATE = By.xpath("//div[@class='map_left_cards__container']/div/a/div/div/div/div/div");
    public static final By FIRTS_HOTEL_REVIEW = By.xpath("//div[@class='map_left_cards__container']/div/a/div/div/div/div/div/div[@class=\"bui-review-score__text\"]");
    public static final By FIRST_HOTEL_PRICE = By.xpath("//div/div/div[@class=\"bui-f-color-destructive js-strikethrough-price prco-inline-block-maker-helper bui-price-display__original bui-price-display__original\"]");
    public static final By FIRST_HOTEL_STARS = By.xpath("//span[@class=\"bui-rating bui-rating--smaller\"][@role=\"img\"]");


    public String getHotelName() {
        return $(FIRST_HOTEL_NAME).getText();
    }
}

