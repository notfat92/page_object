package tests;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.HomePage;
import pages.HotelPage;
import pages.SearchPage;


public class FirstSelenideTest extends BaseTest {

    @Test()
    public void firstTest() {

       String cityName = "Осло";
       HomePage homePage = new HomePage();
       SearchPage searching = new SearchPage();
       HotelPage hotelPage = new HotelPage();
       homePage.openPage().
                acceptCookies().
                findByCity(cityName);
       searching.pageSearch();
     //  String hotelName = hotelPage.getHotelName();
       searching.selectHotel();

    }
}